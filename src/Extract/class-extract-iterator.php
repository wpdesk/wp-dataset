<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WPDesk_Extract_Iterator extends IteratorIterator {
	/** @var WPDesk_Data_Extractor */
	private $extractor;

	public function __construct( Traversable $iterator, WPDesk_Data_Extractor $extractor ) {
		parent::__construct( $iterator );
		$this->extractor = $extractor;
	}

	/**
	 * @return array
	 */
	public function current() {
		return $this->extractor->extract( parent::current() );
	}
}
