<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Can extract associative array from object
 */
interface WPDesk_Data_Extractor {

	/**
	 * Extract array from known object
	 *
	 * @param object $object
	 *
	 * @return array
	 */
	public function extract( $object );
}
