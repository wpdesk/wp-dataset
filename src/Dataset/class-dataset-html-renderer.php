<?php

use WPDesk\View\Renderer\Renderer;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Render dataset as HTML
 */
class WPDesk_Dataset_Html_Renderer implements WPDesk_Dataset_Renderer {

	/** @var string */
	protected $template = '';

	/** @var array */
	protected $template_params = [];

	/** Flexible_Invoices_View */
	protected $view = null;

	/**
	 * Flexible_Invoices_Dataset_Html_Renderer constructor.
	 *
	 * @param Renderer $view
	 * @param $template
	 * @param array $template_params
	 */
	public function __construct( Renderer $view, $template, array $template_params ) {
		$this->template        = $template;
		$this->template_params = $template_params;
		$this->view            = $view;
	}

	public function render_output(
		WPDesk_Dataset $dataset
	) {
		echo $this->render_string( $dataset );
	}

	public function render_string(
		WPDesk_Dataset $dataset
	) {
		return $this->render_template( $this->prepare_args( $dataset ) );
	}

	/**
	 * @param array $args
	 *
	 * @return string
	 */
	protected function render_template( array $args ) {
		return $this->view->render( $this->template, $args );
	}

	/**
	 * @param WPDesk_Dataset $dataset
	 *
	 * @return array
	 */
	protected function prepare_args( WPDesk_Dataset $dataset ) {
		return array_merge( $this->template_params, [
			'dataset' => $dataset,
			'columns' => $dataset->get_header_line()
		] );
	}

}
