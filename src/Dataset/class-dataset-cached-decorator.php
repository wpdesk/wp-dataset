<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPDesk_Dataset_Cached_Decorator implements WPDesk_Dataset {

	/** @var WPDesk_Dataset  */
	private $dataset;
	/**
	 * @param WP_Query $query
	 * @param array $columns
	 */
	public function __construct(WPDesk_Dataset $dataset) {
		$this->dataset = $dataset;
	}

	/**
	 * @return array
	 */
	public function get_header_line() {
		return $this->dataset->get_header_line();
	}

	/**
	 * @return Iterator
	 */
	public function getIterator() {
		/** @noinspection PhpParamsInspection */
		return new CachingIterator($this->dataset->getIterator(), CachingIterator::FULL_CACHE);
	}

}
