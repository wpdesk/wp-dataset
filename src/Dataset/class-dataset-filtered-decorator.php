<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPDesk_Dataset_Filtered_Decorator implements WPDesk_Dataset {

	/** @var WPDesk_Dataset  */
	private $dataset;

	/** @var Iterator */
	private $iterator;

	/**
	 * WPDesk_Dataset_Filtered_Decorator constructor.
	 *
	 * @param WPDesk_Dataset $dataset
	 */
	public function __construct(WPDesk_Dataset $dataset) {
		$this->dataset = $dataset;
		$this->iterator = $dataset->getIterator();
	}

	/**
	 * @return array
	 */
	public function get_header_line() {
		return $this->dataset->get_header_line();
	}

	/**
	 * @return Iterator
	 */
	public function getIterator() {
		return $this->iterator;
	}

	/**
	 * @param callable $filter
	 */
	public function add_filter(callable $filter) {
		$this->iterator = new CallbackFilterIterator($this->iterator, $filter);
	}

}
