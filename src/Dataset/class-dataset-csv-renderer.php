<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Render dataset as CSV
 */
class WPDesk_Dataset_Csv_Renderer implements WPDesk_Dataset_Renderer {

	/** @var string */
	private $filename;

	/**
	 * WPDesk__Dataset_Csv_Renderer constructor.
	 *
	 * @param string $filename
	 */
	public function __construct( $filename ) {
		$this->filename = $filename;
	}

	/**
	 * @param WPDesk_Dataset $dataset
	 */
	public function render_output(
		WPDesk_Dataset $dataset
	) {
		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', false );
		header( 'Content-Type: text/csv; charset=UTF-8' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Disposition: attachment;filename=' . $this->filename );

		echo $this->render_string( $dataset );
	}

	/**
	 * @param WPDesk_Dataset $dataset
	 *
	 * @return string
	 */
	public function render_string(
		WPDesk_Dataset $dataset
	) {
		$csv = \League\Csv\Writer::createFromString( '' );
		$this->set_csv_output_style( $csv );
		$this->add_formatters( $csv );

		$this->insert_header( $csv, $dataset );
		$this->insert_data( $csv, $dataset );

		return $csv->__toString();
	}

	/**
	 * @param \League\Csv\AbstractCsv $csv
	 */
	protected function set_csv_output_style( \League\Csv\AbstractCsv $csv ) {
		$csv->setOutputBOM( \League\Csv\Reader::BOM_UTF8 );
		$csv->setDelimiter( ',' );
	}

	/**
	 * @param \League\Csv\Writer $csv
	 */
	protected function add_formatters( \League\Csv\Writer $csv ) {
		$this->add_formatter_remove_newlines( $csv );
	}

	/**
	 * @param \League\Csv\Writer $csv
	 */
	private function add_formatter_remove_newlines( \League\Csv\Writer $csv ) {
		$csv->addFormatter( function ( $record ) {
			return array_map( function ( $item ) {
				return preg_replace( '/\s+/', ' ', trim( $item ) );
			}, $record );
		} );
	}

	/**
	 * @param WPDesk_Dataset $dataset
	 *
	 * @return array
	 */
	protected function get_header_line( WPDesk_Dataset $dataset ) {
		return $dataset->get_header_line();
	}


	/**
	 * @param \League\Csv\Writer $csv
	 * @param WPDesk_Dataset $dataset
	 */
	protected function insert_header( \League\Csv\Writer $csv, WPDesk_Dataset $dataset ) {
		$header = $this->get_header_line( $dataset );
		$csv->insertOne( $header );
	}

	/**
	 * @param \League\Csv\Writer $csv
	 * @param WPDesk_Dataset $dataset
	 */
	protected function insert_data( \League\Csv\Writer $csv, WPDesk_Dataset $dataset ) {
		$header = $this->get_header_line( $dataset );
		foreach ( $dataset as $item ) {
			$item_filtered = [];
			foreach ( $header as $header_key => $value ) {
				if (key_exists($header_key, $item)) {
					$item_filtered[] = $item[ $header_key ];
				} else {
					$item_filtered[] = '';
				}
			}
			$csv->insertOne( $item_filtered );
		}
	}
}
