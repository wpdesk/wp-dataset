<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Can render dataset using object extractor
 */
interface WPDesk_Dataset_Renderer {

	/**
	 * Outputs rendered dataset to standard output
	 *
	 * @param WPDesk_Dataset $dataset
	 *
	 * @return void
	 */
	public function render_output( WPDesk_Dataset $dataset );

	/**
	 * Renders dataset to string
	 *
	 * @param WPDesk_Dataset $dataset
	 *
	 * @return string
	 */
	public function render_string( WPDesk_Dataset $dataset );
}
