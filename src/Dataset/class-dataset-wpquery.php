<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WPDesk_Dataset_WpQuery implements WPDesk_Dataset {

	/** @var WP_Query */
	private $query;

	/** @var array */
	private $header;

	/**
	 * @param WP_Query $query
	 * @param array $columns
	 */
	public function __construct( WP_Query $query, array $columns ) {
		$this->query  = $query;
		$this->header = $columns;
	}

	/**
	 * @return array
	 */
	public function get_header_line() {
		return $this->header;
	}

	/**
	 * @return ArrayIterator
	 */
	public function getIterator() {
		$array = new ArrayObject( $this->query->get_posts() );

		return $array->getIterator();
	}


}
