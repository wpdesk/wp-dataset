<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

interface WPDesk_Dataset extends IteratorAggregate {

	/**
	 * Returns associative array with header. Each value is a column
	 *
	 * @return array
	 */
	public function get_header_line();
}
