<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WPDesk_Dataset_Extract_Decorator implements WPDesk_Dataset {

	/** @var WPDesk_Dataset */
	private $dataset;

	/** @var WPDesk_Data_Extractor */
	private $extractor;

	/**
	 * WPDesk_Dataset_Extract_Decorator constructor.
	 *
	 * @param WPDesk_Dataset $dataset
	 * @param WPDesk_Data_Extractor $extractor
	 */
	public function __construct( WPDesk_Dataset $dataset, WPDesk_Data_Extractor $extractor ) {
		$this->dataset = $dataset;
		$this->extractor = $extractor;
	}

	/**
	 * @return array
	 */
	public function get_header_line() {
		return $this->dataset->get_header_line();
	}

	/**
	 * @return Iterator
	 */
	public function getIterator() {
		return new WPDesk_Extract_Iterator( $this->dataset->getIterator(), $this->extractor );
	}


}
